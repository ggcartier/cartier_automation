package com.framework;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
Properties pr;
	
	public PropertyReader(String filePath){
		pr = new Properties();
	
	try {
		pr.load(new FileInputStream(new File(filePath)));
		String Browser = pr.getProperty("Browser");
		
		System.out.println(Browser);
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	public String getValue(String key){
		return pr.getProperty(key);
	}

}

