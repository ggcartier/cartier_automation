package com.framework;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class BaseDriver {

	public WebDriver driver;
	
	

	public void launchBrowser(String browser) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "\\src\\main\\resources\\chromedriver.exe");
			driver = new ChromeDriver();	
			
		}
		else if(browser.equalsIgnoreCase("firefox")){
			
			System.setProperty("webdriver.gecko.driver", "");
			driver=new FirefoxDriver();
			
		}
		else if (browser.equalsIgnoreCase("ie")) {
			System.setProperty("", "");
			driver = new InternetExplorerDriver();
			
			
		}
		
		else
		{
			
			System.out.println("Enter a valid browser name");
		}
		
	
		
	}

	public void closeBrowser() {
		
		
		driver.close();;
	}
	public WebDriver getDriver() {
		System.out.println("________"+driver);
		return driver;
	}
	
	public void setDriver(WebDriver dri) {
		driver=dri;
		System.out.println("{+++++++++"+driver);
	}
	
}
