package com.sapient.cartier.stepdefination;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.framework.BaseDriver;
//import com.framework.BaseClass;

import Pages.Shopping_Bag;
import Pages.site_map_links;
import Pages.Aem_Search;
import Pages.Delivery_Page;
import Pages.Home_Page;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class stepdefination extends BaseDriver {

	private String url;

	private By cancel_popup = By.xpath("//button[@class='close']");
	@Before
	public void OpenBrowser() throws InterruptedException 	
	{
			
			//System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
			//driver = new ChromeDriver();
			//BaseDriver launch = new BaseDriver();
		    launchBrowser("chrome");
		    //url = "https://www.preprod2.cartier.cn/";
		    //url = "https://www.quality.cartier.com/";
		    //url = "https://secure-cartier-desktop-projqa.sapient.cn?q=123/";
		    url = "https://www.cartier.com/";		    	
		    driver.get(url);
			//driver.get("https://www.phptravels.net/");
			driver.manage().window().maximize();
			Thread.sleep(2000);
			
			if (url.equalsIgnoreCase("https://www.cartier.com/"));
				{
					driver.findElement(cancel_popup).click();
				}
					
					/*JavascriptExecutor js = (JavascriptExecutor)driver;			
					js.executeScript("arguments[0].scrollIntoView();", scroll);
					*/
			
	} 
	@After
	 public void CloseBrowser() 
	 {
		 driver.close();
		 driver.quit();
	 }

	
	@Given("^User is on home page$")
	public void home_page() throws InterruptedException 
	{
		System.out.println("homepage");
	}
	
	@When("^User selects item and redirected to shopping bag$")
	public void add_product() throws InterruptedException 
	{
		Shopping_Bag product = new Shopping_Bag(driver, url);
		product.cartier_sb();
	
	}
	
	@Then("^After submitting User should be redirected to delivery page where Individual form should be selected bydefault$")
	public void company_radio() throws InterruptedException 
	{
		Delivery_Page dp = new Delivery_Page(driver);
		dp.company_selection();
		
	}	
	
	/*@When("^efapio form gets filled$")
	public void delivery_page() throws InterruptedException 
	{
		Delivery_Page dp = new Delivery_Page(driver);
		dp.eFapiao_form();
		
	}	*/

	
	
	@When("^User selects Company radio button and doesnt enter tax code while entering efapiao form$")
	public void eFapiao_form_without_tax() throws InterruptedException 
	{
		Delivery_Page dp = new Delivery_Page(driver);
		dp.eFapiao_form_without_tax();
	

	}
	@Given ("^User is on delivery page, enters all shipping details$")
	public void shipping_form() throws InterruptedException 
	{
		Delivery_Page dp = new Delivery_Page(driver);
		dp.shipping_form();
		
	}
		
	@Then("^tax code should show error message$")
	public void tax_code_check() throws InterruptedException 
	{
		Delivery_Page dp = new Delivery_Page(driver);
		//dp.tax_code_check();		
	

	}
	
	@When("^User submits company form and comes back from payment page$")
	public void eFapiao_form() throws InterruptedException 
	{
		Delivery_Page dp = new Delivery_Page(driver);
		dp.back_to_shipping_from_payment();
	

	}
	
	@Then("^form details should retain$")
	public void retention() throws InterruptedException 
	{
		Delivery_Page dp = new Delivery_Page(driver);
		dp.retention();
		
	}
	
	
	
	//////////////////>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<????????????????????
	
	
	
	
	
		
	@When("^User opens sitemap links and clicks on each link$")
	public void open_site_map() throws InterruptedException 
	{
		site_map_links site_map = new site_map_links(driver, url);
		site_map.open_sitemap();		
	

	}
	
	@Then("^User gets response$")
	public void link_response() throws InterruptedException 
	{
		site_map_links site_map = new site_map_links(driver, url);
		site_map.URLCheckTest();
	

	}
	
		
	@When("^User enter any keyword on search field and press enter$")
	public void user_enter_any_keyword() throws Throwable {
	    
		Home_Page home = new Home_Page(driver);
		home.aem_search_field();
	}


	@Then("^User should be redirected to search results Page$")
	public void user_should_be_redirected_to_search_results_Page() throws Throwable {
	    System.out.println("User successfully redirected to search page");
	    
	}

	@Given("^User is on AEM search results page$")
	public void user_is_on_AEM_search_results_page() throws Throwable {
		System.out.println("User is on aem search results page");
	
	}

	@When("^User clicks on view more products on any bucket$")
	public void user_clicks_on_view_more_products_on_any_bucket() throws Throwable {
		Aem_Search search=new Aem_Search(driver);
		Thread.sleep(2000);
		search.aem_search_page();
	   
	}

	@Then("^Few more products opened$")
	public void some_more_products_opened() throws Throwable {
		System.out.println("Few more products opened");
	    
	}

	@Then("^User verifies all product properties$")
	public void user_verifies_all_product_properties() throws Throwable {
		Aem_Search search=new Aem_Search(driver);
		Thread.sleep(2000);
		search.search_reults_page_productverfication();
	
	}

	@When("^User clicks on any of the product$")
	public void user_clicks_on_any_of_the_product() throws Throwable {
		Aem_Search search=new Aem_Search(driver);
		Thread.sleep(2000);
		search.search_reults_page_productclick();
	
	}

	@Then("^PDP page opens$")
	public void pdp_page_opens() throws Throwable {
	    System.out.println("PDP page opens successfully from search results page");

	}
	@When("^User selects Sold Online radio button$")
	public void User_selects_Sold_Online_radio_button() throws Throwable {
		Aem_Search search=new Aem_Search(driver);
		Thread.sleep(2000);
		search.search_sold_online();
		System.out.println("Sold online filter has been selected");
	
	}

	@Then("^All sellable items are showing$")
	public void All_sellable_items_are_showing() throws Throwable {
	    System.out.println("All sellable items are selected");

	}
	
	@When("^User checks vignettes availability$")
	public void User_check_vignettes() throws Throwable {
		Aem_Search search=new Aem_Search(driver);
		Thread.sleep(2000);
	   search.vignette_search();
	}

	@Then("^User clicks on Vignettes for available vignettes$")
	public void User_click_vignette() throws Throwable {
		System.out.println("Vignette has been clicked and PDP opens");
	    
	}

	@Then("^User check for more information text$")
	public void user_check_for_more_information_text() throws Throwable {
		Aem_Search search=new Aem_Search(driver);
		search.vignette_text();
	}
	}
