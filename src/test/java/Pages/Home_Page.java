package Pages;

import java.awt.Desktop.Action;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.xpath.XPath;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.interactions.internal.Locatable;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.testng.Assert;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

//import com.framework.BaseClass;
import com.framework.BaseDriver;

//import testsession.runners.PropertyReader;

public class Home_Page extends BaseDriver {
	//private By PFPage1Heading = By.xpath("//*[@class='heading1 pf-boutique-heading']");
	WebDriver driver = null;
	String url;
	
	//************************   X Path library starts   **********************//
	private By cancel_popup = By.xpath("//button[@class='close']");
	private By click_myaccount = By.xpath("//span[@data-loggedouttext='Your account']");
	private By Hover = By.xpath("(//a[@role='button'])[6]");
	private By scroll = By.xpath("//p[@class='c-reassurance-footer__label']");
	private By tank_click = By.xpath("(//a[@class='clearfix' and @data-ga-third-level='Tank'])[1]");
//	@FindBy(xpath="//button[@class='btn-add-to-basket loader-btn']")
//	private WebElement add_to_basket_pdp;
	private By add_to_basket_pdp = By.xpath("//button[@class='btn-add-to-basket loader-btn']");
	//private By aemsearch = By.id("js-search-form-input");
	private By aemsearch =By.id("js-search-form-input");
	
	private By click_tank = By.xpath("//html[@class='cssanimations csspseudoanimations csscolumns csscolumns-width csscolumns-span csscolumns-fill csscolumns-gap csscolumns-rule csscolumns-rulecolor csscolumns-rulestyle csscolumns-rulewidth csscolumns-breakbefore csscolumns-breakafter csscolumns-breakinside flexbox flexboxlegacy no-flexboxtweener flexwrap']");
	private By view_basket = By.xpath("//i[@class='nav-link-icon icon icon-sb']");
	private By add_to_bag_cn = By.xpath("//button[@data-href='/zh-cn/collections/watches/mens-watches/tank/tank-solo/w5200028-tank-solo-watch.html']");
	private By add_to_bag = By.xpath("(//button[@class='buy-btn js-addToShopping'])[2]");
	//private By add_to_basket_pdp = By.xpath("//div[@class='c-pdp__cta-section--btn-add-to-basket js-pdp__cta-section--btn-add-to-basket c-pdp__cta-section--btns btn-span-xs']");
	private By click_basket = By.cssSelector("a[class='c-header__backet-btn-wrapper']");
	
	private By click_viewall = By.xpath("//a[@class='c-collection-link']");
	
	//************************   X Path library ends   **********************//
	
	public Home_Page(WebDriver driver){
	
		this.driver= driver;
		
	} 
	
	/*public void clickElementWhenClickable(By locator, int timeout) {
	    WebElement element = null;
	    WebDriverWait wait = new WebDriverWait(driver, timeout);
	    element = wait.until(ExpectedConditions.elementToBeClickable(locator));
	    element.click();
	}
	  */  
	
	public void javascriptClick(WebElement element) throws InterruptedException { 
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    System.out.println(tabs.size());
	    driver.switchTo().window(tabs.get(1)); 
	WebElement element1 = driver.findElement(By.xpath("(//a[@class='c-collection-link'])[1]"));
	
	JavascriptExecutor jex = (JavascriptExecutor)driver;
	jex.executeScript("arguments[0].scrollIntoView(true)",element1);
	
	
	
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

	
	jex.executeScript("arguments[0].click();",element1);
	}
		
	//System.out.println("***inside java script click");
	
	public void cartier_home_page() throws InterruptedException
	    {
		
		//cancel_popup();
		
	   	mega_menu_selection();
		
	   	add_items_to_cart();
		
		}
	
	public void aem_search_field() throws InterruptedException 
	{
		System.out.println("aem search field");
		Thread.sleep(2000);
		driver.findElement(aemsearch).sendKeys("tank");
		driver.findElement(aemsearch).submit();
	}
	
	private void add_items_to_cart() throws InterruptedException {
		
				
		//System.out.println("tab shift");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		javascriptClick(null);	
	
		/*WebElement element = driver.findElement(By.className("c-collection-link"));		
		WebDriverWait wait=new WebDriverWait(driver, 50);
		//By click_tank_solo = By.xpath("//a[@class='c-collection-link']");

		wait.until(ExpectedConditions.elementToBeClickable(By.className("c-collection-link"))); 
		element.click();*/
		System.out.println("hi *************");
		//WebElement element1 = driver.findElement(By.xpath("//a[@class='c-collection-link']"));

		/*Actions actions = new Actions(driver);

		actions.moveToElement(element).click().perform();*/
		
		/*if (element1.isDisplayed())
		{		
		   System.out.println("inside display function***************");
			
			javascriptClick(element1);
			//this.driver.findElement(click_tank_solo).click();
		}*/
		
		/* Actions actions = new Actions(driver);
		WebElement we = driver.findElement(click_tank_solo);
		actions.moveToElement(we);
		*/
		/*Coordinates cor = ((Locatable)click_tank_solo).getCoordinates();
		cor.onPage();
		cor.inViewPort();*/
		
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    System.out.println(tabs.size());
	    driver.switchTo().window(tabs.get(2));

		JavascriptExecutor js = (JavascriptExecutor)driver;
		WebElement Ele = driver.findElement(By.xpath("(//a[@data-ga-product-ref='W5200028'])[1]"));
		js.executeScript("arguments[0].scrollIntoView();", Ele);

		js.executeScript("arguments[0].click();",Ele);
		
		/*ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    System.out.println(tabs.size());*/
		
		ArrayList<String> tabs1 = new ArrayList<String> (driver.getWindowHandles());
	    System.out.println(tabs.size());
	    driver.switchTo().window(tabs1.get(3));
		
		
		/*System.out.println("add_to_basket_pdp");
		  WebDriverWait wait=new WebDriverWait(driver,50);
		  wait.until(ExpectedConditions.elementToBeClickable(add_to_basket_pdp));*/
//		Assert.assertTrue((add_to_basket_pdp).isDisplayed());
//	    Thread.sleep(10000);
//		this.driver.findElement(add_to_basket_pdp).isSelected();
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(add_to_basket_pdp));
			 
	  
		//add_to_basket_pdp.click(); 
	    WebElement ele1 = driver.findElement((By) add_to_basket_pdp);
		//waitForElementVisibility(5, ele1);
	    JavascriptExecutor js1 = (JavascriptExecutor)driver;
		js1.executeScript("arguments[0].scrollIntoView();", ele1);
		Thread.sleep(5000);
		js1.executeScript("arguments[0].click();",ele1);
	   // 	WebDriverWait wait =  new WebDriverWait(driver,7);
	    //	wait.until(ExpectedConditions.elementToBeClickable(ele1));
	    //driver.findElement(add_to_basket_pdp).click();
	    //driver.findElement(add_to_basket_pdp).click();
	//    	JavascriptExecutor js1 = (JavascriptExecutor)driver;
	    	//js1.executeScript("arguments[0].scrollIntoView();", ele1);
	  //  	js1.executeScript("arguments[0].click();",ele1);
	    	//ele1.click();
	    	//ele1.click();
		
		//this.driver.findElement(Ele).click();
		
		Thread.sleep(10000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Actions actionobj1 = new Actions(driver);
		
		WebElement element11 = driver.findElement(view_basket);
		
		wait.until(ExpectedConditions.elementToBeClickable(element11));
		actionobj1.moveToElement(element11).build().perform();
		//Thread.sleep(5000);
		waitForElementVisibility(10,driver.findElement(click_basket));
		
		this.driver.findElement(click_basket).click();
	}
	
	public void waitForElementVisibility(int timeInSec, WebElement elem) {
		WebDriverWait wait = new WebDriverWait(driver, timeInSec);
		wait.until(ExpectedConditions.visibilityOf(elem));		
	}
	
	private void mega_menu_selection() throws InterruptedException {
		Actions actionobj = new Actions(driver);
	    WebElement element = this.driver.findElement(Hover);
		actionobj.moveToElement(element).build().perform();
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Thread.sleep(3000);
		this.driver.findElement(tank_click).isDisplayed();

		this.driver.findElement(tank_click).click();
		
	}
	private void cancel_popup() {
		System.out.println("hi1");
		if (url.equalsIgnoreCase("https://www.cartier.com")) {
			System.out.println("hi2");	
			this.driver.findElement(cancel_popup).click();
			
			/*JavascriptExecutor js = (JavascriptExecutor)driver;			
			js.executeScript("arguments[0].scrollIntoView();", scroll);
			*/
			
		}
	}
	

}
