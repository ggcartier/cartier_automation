package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.framework.BaseDriver;

public class Aem_Search extends BaseDriver{
	WebDriver driver;

public Aem_Search(WebDriver driver){
		
		this.driver= driver;
		
	}

public void aem_search_page() throws InterruptedException
{
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	//driver.findElement(By.cssSelector("view-more")).click();
	//driver.findElement(By.xpath("//div[@id='content']/form[2]/div/div[4]/div/div/span[2]"));
	driver.findElement(By.xpath("//span[@class='view-more']"));
	Thread.sleep(2000);
}

public void search_reults_page_productverfication() throws InterruptedException
{
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	//driver.findElement(By.cssSelector("view-more")).click();
	//WebElement element=driver.findElement(By.xpath("//img[@alt='Tank Solo watch Small model, steel']"));
	//String s=element.getAttribute("src");
	//System.out.println("Image source is"+ s);
	WebElement element=driver.findElement(By.xpath("//div[@data-alt='Tank Solo watch Small model, steel']"));
	String s=element.getAttribute("data-original-url");
	System.out.println("Original Image source is:"+" "+s);
	
	//WebElement e=driver.findElement(By.xpath("//div[@class='pushes-wrapper_push js-pushes-wrapper_push js-scrollga-enable']"));
	WebElement e=driver.findElement(By.xpath("(//div[@class='pushes-wrapper_push js-pushes-wrapper_push js-scrollga-enable'])[5]"));
	//WebElement e=driver.findElement(By.xpath("(//div[@class='pushes-wrapper_push js-pushes-wrapper_push js-scrollga-enable']/a[@href='/en-us/collections/watches/women-s-watch/tank/tank-solo/w5200013-tank-solo-watch.html#CRW5200013']"));
	String s1=e.getAttribute("data-ee-products");
	System.out.println("Properties of Product verified is:"+" "+s1);
	/*Actions action = new Actions(driver);	 
    action.moveToElement(e).perform();
    WebElement subElement = driver.findElement(By.linkText("W5200013"));

    action.moveToElement(subElement);
    action.click();
    action.perform();
	Thread.sleep(2000);
	System.out.println("Product page opens");*/
}

public void search_reults_page_productclick() throws InterruptedException
{
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	//driver.findElement(By.cssSelector("view-more")).click();
	//driver.findElement(By.xpath("//div[@id='content']/form[2]/div/div[4]/div/div/span[2]"));
	WebElement e=driver.findElement(By.xpath("//img[@alt='Tank Solo watch Small model, steel']"));
	e.click();
		//driver.findElement(By.xpath("//div[@class='pushes-wrapper_push js-pushes-wrapper_push js-scrollga-enable']")).click();
	Thread.sleep(2000);
	System.out.println("PDP page opens");
	String s= driver.getTitle();
	System.out.println("title of PDP is:"+ " " +s);
	
}

public void search_sold_online()
{
	WebElement e=driver.findElement(By.xpath("//input[@id='sold-online']"));
	e.click();
}

public void vignette_search()
{
	boolean b;
	WebElement ele=driver.findElement(By.xpath("(//span[@class='prod-strap-link']/img[@src='/content/dam/cartier_dam/straps/vignette-images/KD6UXAGA.png'])[1]"));
	b=ele.isDisplayed();
	if (b==true)
	{
		System.out.println("Vignettes are available");
	}
	else
	{
		System.out.println("Vignettes are not available on any PDP");
	}
	
}

public void vignette_click()
{
	WebElement ele=driver.findElement(By.xpath("(//span[@class='prod-strap-link']/img[@src='/content/dam/cartier_dam/straps/vignette-images/KD6UXAGA.png'])[1]"));
	ele.click();
}

public void vignette_text()
{
	boolean b;
	WebElement ele=driver.findElement(By.xpath("(//span[@class='prod-strap-message'])[1]"));
	b=ele.isDisplayed();
	if (b==true)
	{
		System.out.println("Vignettes Text is available");
	}
	else
	{
		System.out.println("Vignettes Text is not available");
	}
}
}
