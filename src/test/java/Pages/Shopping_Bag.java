package Pages;

import java.awt.Desktop.Action;
import java.sql.Array;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
//import org.testng.Assert;
import org.openqa.selenium.support.ui.Select;

//import com.framework.BaseClass;
import com.framework.BaseDriver;

import testsession.runners.PropertyReader;

public class Shopping_Bag extends BaseDriver {
	//private By PFPage1Heading = By.xpath("//*[@class='heading1 pf-boutique-heading']");
	WebDriver driver;
	
	//************************   X Path library starts   **********************//
	
	private By count_dropdown = By.xpath("//select[@class='c-select--el js-select--el  js-quantityselect']");
	private By checkbox_engraving = By.xpath("(//a[@class='js-personalization-title c-personalization--title-r'])[1]");
	private By fill_engraving_text_1 = By.xpath("//input[@id='js-max-length-1_0']");
	private By fill_engraving_text_2 = By.xpath("//input[@id='js-max-length-3_0']");
	private By save_eng = By.xpath("//*[@id='content']/div[1]/div[6]/div/div/div/ul/li/form/fieldset/ul/li[2]/div[2]/div/div[1]/div[2]/div[2]/div[3]/button");
	private By continue_button = By.xpath("//*[@id='content']/div[1]/div[6]/div/div/div/div[2]/div/div/button[2]");
	//private By continue_cn = By.xpath("//*[@id='content']/div[1]/div[6]/div/div/div/div[2]/div/div/button[2]");
	
	private By continue_cn = By.xpath("//ul[@class='js-productlist-inbag']/following-sibling::div/div/div/button[2]");
	private By guest_flow = By.xpath("//*[@class='text-align-center']");
	//private By F_Name = By.xpath("//*[@id='shippingForm_firstName1']");
	private By F_Name = By.id("shippingForm_firstName1");
	private By L_Name = By.xpath("//*[@id='shippingForm_lastName1']");
	private By Add_1 = By.xpath("//*[@id='shippingForm_address2']");
	private By Add_2 = By.xpath("//*[@id='shippingForm_address5']");
	private By Add_3 = By.xpath("//*[@id='shippingForm_address7']");
	private By Add_zip= By.xpath("//*[@id='shippingForm_zip']");
	private By Add_4= By.xpath("//*[@name='shippingForm_address9']");

	String url;
	
	//************************   X Path library ends   **********************//
	
	
	public Shopping_Bag(WebDriver driverpassed, String url){
		
		this.driver= driverpassed;
		this.url = url;
	} 
	public void cartier_sb() throws InterruptedException{
		System.out.println("hi");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    System.out.println(tabs.size());
	    driver.switchTo().window(tabs.get(4));
	 /* if (url.equalsIgnoreCase("https://www.kljkj"))
	   {*/
			  
		WebElement dropdown = this.driver.findElement(count_dropdown);
		
		Select selectobj = new Select(dropdown);
		selectobj.selectByVisibleText("3"); 
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//this.driver.findElement(checkbox_engraving).click();
		//Thread.sleep(5000);
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//this.driver.findElement(By.xpath("//*[@id='js-max-length-1_0']")).sendKeys("My passion1");
		//this.driver.findElement(By.xpath("//*[@id='js-max-length-3_0']")).sendKeys("My passion2");
		//this.driver.findElement(By.xpath("//button[@data-button-form='engraving']")).click();
		/*this.driver.findElement(fill_engraving_text_2).sendKeys("My Love");
		this.driver.findElement(save_eng).click();*/
		//}
	    /*driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    WebElement ele1 = driver.findElement(By.xpath("//button[@class='cta-button cta-button__inner cta--red-width-auto-pad-20 js-login-checkout-cta']"));
	    JavascriptExecutor jce = (JavascriptExecutor)driver;
	    jce.executeScript("arguments[0].scrollIntoView();", ele1);
	    */
	    Thread.sleep(2000);
		this.driver.findElement(continue_cn).click();
		Thread.sleep(5000);
		this.driver.findElement(guest_flow).click();
		
		/*this.driver.findElement(F_Name).sendKeys("Ashish");
		this.driver.findElement(L_Name).sendKeys("Bhatia");*/
		
		}
	

}
