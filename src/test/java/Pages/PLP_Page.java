package Pages;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import com.framework.BasePage;

public class PLP_Page {

	public PLP_Page(WebDriver driverpassed){
		this.driver= driverpassed;
	}
        
        WebDriver driver;
        String plpModelName;
        String plpRefDesc;
        String plpRefPrice;
        String plpReftaxstate;
        Boolean plpRefAddSb;
        //Xpath for PLP page
        private By popinclose = By.xpath("//button[@class='close']");
        //private By plpreferenceImage=By.xpath("//img[@alt='Tank Solo watch']/../../../../../../div[2]/div[1]/a/div[1]/div[1]/img");
        String src;
        private By plpreferenceName = By.xpath("//html//div[2]/div[1]/a[1]/div[2]/h3[1]");
        private By plpimg = By.xpath("//h3[@class='prod-name']/../../../../../div[2]/div[1]/a/div[1]/div");
        private By plpreferenceshortDesc = By.xpath("//html//div[2]/div[1]/a[1]/div[2]/span[1]");
        //private By ReferenceNum = By.xpath("//div[@class='c-pdp__cta-section--product-ref-id js-pdp__cta-section--product-ref-id']");
    	private By plprefAddSbButton = By.xpath("//button[@class='buy-btn js-addToShopping']");
    	private By plpreferencePrice = By.xpath("//html//div[2]/div[1]/a[1]/div[2]/div[1]/div[1]");
    	private By plpreferencePricetaxstate = By.xpath("//div[@class='tax-message']");
    	 
    	//Xpath for PDP Page
    	private By pdpreferenceName = By.xpath("//span[@class='c-pdp__cta-section--product-title js-pdp__cta-section--product-title']");
        private By pdpreferenceSize = By.xpath("//span[@class='c-pdp__cta-section--product-tags js-pdp__cta-section--product-tags']");
        private By pdpreferenceNum = By.xpath("//div[@class='c-pdp__cta-section--product-ref-id js-pdp__cta-section--product-ref-id']");
    	private By pdpaddSbButton = By.xpath("//div[@class='c-pdp__cta-section--btn-add-to-basket js-pdp__cta-section--btn-add-to-basket c-pdp__cta-section--btns btn-span-xs']");
    	private By pdpreferencePrice = By.xpath("//div[contains(@class,'price js-product-price-formatted')]");
    	private By pdpreferencePricetaxstate = By.xpath("//div[contains(@class,'inc-text js-inc-text')]");
    	
    	/*public void ClickButton()
    	{
    
    		this.driver.findElement(AddSbButton).click();
    		
    	}*/
    	
    	public void PopInClose()
    	{
    		Boolean popin;
    		popin = driver.findElement(popinclose).isDisplayed();
    			if ( popin == true )
    				{
	    				driver.findElement(popinclose).click();
	    				JavascriptExecutor js = (JavascriptExecutor) driver;
	    	            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	    	            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	    	            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    				} 
    	         else
    	            {
    	            	System.out.println("Dispatch Popin Not available to close");
    	            	JavascriptExecutor js = (JavascriptExecutor) driver;
        	            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        	            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        	            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    	            }
    			
    	}
    	
    	public void Openpdp()
    	{
    		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    		WebElement element = this.driver.findElement(plpimg);
			Actions action = new Actions(driver);
			action.moveToElement(element).moveToElement(driver.findElement(plpimg)).click().perform();
    		/*Actions action = new Actions(driver);
    		action.moveToElement(plpreferenceName).click().perform();
    		//this.driver.findElement(plpreferenceName).click();
*/    	}
    	
    	public void PlpVerifyText()
    	{
    		//driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    		//src = this.driver.findElement(plpreferenceImage).toString();
    		//System.out.println(src);
    		WebDriverWait wait = new WebDriverWait(driver, 20);
    		wait.until(ExpectedConditions.visibilityOfElementLocated(plpreferenceName));
    		plpModelName = this.driver.findElement(plpreferenceName).getText();
    		System.out.println(plpModelName);
    		plpRefDesc = this.driver.findElement(plpreferenceshortDesc).getText();
    		System.out.println(plpRefDesc);
    		plpRefAddSb = this.driver.findElement(plprefAddSbButton).isDisplayed();
    		System.out.println(plpRefAddSb);
    		plpRefPrice = this.driver.findElement(plpreferencePrice).getText();
    		System.out.println(plpRefPrice);
    		plpReftaxstate = this.driver.findElement(plpreferencePricetaxstate).getText();
    		System.out.println(plpReftaxstate);
    		    		
    	}
    	
    	public void PDPVerifyText()
    	{
    		
    		String pdpModelName = this.driver.findElement(pdpreferenceName).getText();
    		System.out.println(pdpModelName);
    		String pdpRefSize = this.driver.findElement(pdpreferenceSize).getText();
    		System.out.println(pdpRefSize);
    		String pdpRefNum = this.driver.findElement(pdpreferenceNum).getText();
    		System.out.println(pdpRefNum);
    		String pdpRefPrice = this.driver.findElement(pdpreferencePrice).getText();
    		System.out.println(pdpRefPrice);
    		String pdpReftaxstate = this.driver.findElement(pdpreferencePricetaxstate).getText();
    		System.out.println(pdpReftaxstate);
    		Boolean pdpaddtosb=this.driver.findElement(pdpaddSbButton).isDisplayed();
    		System.out.println(pdpaddtosb);
    		System.out.println("PDP Reference Model Name is same as of PLP page and is " + pdpModelName+ " " + plpModelName.equals(pdpModelName));
    		System.out.println("PDP short description is same as of PLP page and is " + pdpRefSize + " " + plpRefDesc.equals(pdpRefSize));
    		System.out.println("PDP Reference Price is same as of PLP page and is " + pdpRefPrice + " " + plpRefPrice.equals(pdpRefPrice));
    		System.out.println("PDP Tax state is same as of PLP page and is " + pdpReftaxstate + " " + plpReftaxstate.equals(pdpReftaxstate));
    		System.out.println("PDP Add to SB button is present on PLP page and is "+ " " + plpRefAddSb.equals(pdpaddtosb));
    		
    	}
}
