package Pages;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import com.utils.PropertyReader;

public class PDP_Page {

WebDriver driver;
	
	
	String ModelNamePDP, RefSizePDP, RefNumPDP, RefPricePDP, ReftaxstatePDP;
	String mcheadername, mcReferenceDetails, mcReferenceSize, mcReferencePrice, mcReferenceItemsAddedNumber, mcTotalAmount, mcReferencePricetaxstate, mcCheckoutButton;
	String ModelName;
	String RefSize;
	String RefPrice;
	String Reftaxstate;
	String ModelNameMC;
	String RefSizeMC;
	String RefPriceMC;
	String ReftaxstateMC;	
	Boolean popin_visible;
	String DOLowerCase;
	String ROShippingAddressFullname;
	String plpModelName;
    String plpRefDesc;
    String plpRefPrice;
    String plpReftaxstate;
    Boolean plpRefAddSb;
    
    //Xpath for PLP page
    String src;
    private By plpreferenceName = By.xpath("//html//div[2]/div[1]/a[1]/div[2]/h3[1]");
    private By plpimg = By.xpath("//h3[@class='prod-name']/../../../../../div[2]/div[1]/a/div[1]/div");
    private By plpreferenceshortDesc = By.xpath("//html//div[2]/div[1]/a[1]/div[2]/span[1]");
	private By plprefAddSbButton = By.xpath("//button[@class='buy-btn js-addToShopping']");
	private By plpreferencePrice = By.xpath("//html//div[2]/div[1]/a[1]/div[2]/div[1]/div[1]");
	private By plpreferencePricetaxstate = By.xpath("//div[@class='tax-message']");
	 
	//Xpath for PDP Page Gaurav Script
	private By pdpreferenceName = By.xpath("//span[@class='c-pdp__cta-section--product-title js-pdp__cta-section--product-title']");
    private By pdpreferenceSize = By.xpath("//span[@class='c-pdp__cta-section--product-tags js-pdp__cta-section--product-tags']");
    private By pdpreferenceNum = By.xpath("//div[@class='c-pdp__cta-section--product-ref-id js-pdp__cta-section--product-ref-id']");
	private By pdpaddSbButton = By.xpath("//div[@class='c-pdp__cta-section--btn-add-to-basket js-pdp__cta-section--btn-add-to-basket c-pdp__cta-section--btns btn-span-xs']");
	private By pdpreferencePrice = By.xpath("//div[contains(@class,'price js-product-price-formatted')]");
	private By pdpreferencePricetaxstate = By.xpath("//div[contains(@class,'inc-text js-inc-text')]");

    //xpaths of the elements on PDP page
    private By popinclose = By.xpath("//div[@class='modal-dialog modal-lg']//div[@class='modal-content']//div[@class='modal-header']//button[@type='button']");
    private By ReferenceName = By.xpath("//span[@class='c-pdp__cta-section--product-title js-pdp__cta-section--product-title']");
    private By ReferenceSize = By.xpath("//span[@class='c-pdp__cta-section--product-tags js-pdp__cta-section--product-tags']");
    private By ReferenceNum = By.xpath("//div[@class='c-pdp__cta-section--product-ref-id js-pdp__cta-section--product-ref-id']");
	private By AddSbButton = By.xpath("//*[@class='c-pdp__cta-section--btn-add-to-basket js-pdp__cta-section--btn-add-to-basket c-pdp__cta-section--btns btn-span-xs']");
	private By ReferencePrice = By.xpath("//div[contains(@class,'price js-product-price-formatted')]");
	private By ReferencePricetaxstate = By.xpath("//div[contains(@class,'inc-text js-inc-text')]");
	private By StrapConfiguratorPopin = By.xpath("//*[@class='c-pdp__strap-config--confirmbox-bg js-pdp__strap-config--confirmbox-bg']");
	private By StrapPopinCheckoutButton = By.xpath("//div[@class='c-pdp__strap-config--confirmbox-ctablock']/button[@class='c-pdp__strap-config--confirmbox-shoppingbag js-pdp__strap-config--confirmbox-shoppingbag']");
	
	//xpaths of the elements on Mini Cart
	private By EmptyMinicartIcon = By.xpath("//*[@class='iconbox__number js-cart-count']");
	private By MinicartIcon = By.xpath("//li[@class='js-shopping-bag visible-m visible-d nav__list-item']");
	private By MinicartHeaderText = By.xpath("//h2[@class='c-header__basket-title js-basket-title']");
	private By MinicartReferenceName = By.xpath("//*[@class='c-header__basket-list-item__product-name js-basket-product-name']");
	private By MinicartReferenceSize = By.xpath("//*[@class='c-header__basket-list-item__product-description js-basket-product-description']");
	private By MinicartReferencePrice = By.xpath("//*[@class='c-header__basket-list-item__amount js-basket-product-amount']");
	private By MinicartReferenceItemsAddedNumber = By.xpath("//*[@class='c-header__basket-list-item__total-product js-basket-product-count']");
	private By MinicartTotalAmount = By.xpath("//*[@class='c-header__basket__total-price js-basket-total-amount']");
	private By MinicartReferencePricetaxstate = By.xpath("//*[@class='c-header__basket__tax-info js-basket-tax-info']");
	private By MinicartCheckoutButton = By.xpath("//*[@class='c-header__backet-btn']");
	private By MiniCartIconView = By.xpath("//*[@class='c-header__cart-wrapper']");

		
	public PDP_Page(WebDriver driverpassed){
		this.driver= driverpassed;
	}
	
	
	public void ClickButton()
	{
		
		this.driver.findElement(AddSbButton).click();
		boolean popinbox;
		popinbox=this.driver.findElement(StrapConfiguratorPopin).isDisplayed();
			
		if (popinbox == true) 
			{	
				driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
				this.driver.findElement(StrapPopinCheckoutButton).click();
				System.out.println("Strap Configurator button is clicked");
			}
		else 
			{
				System.out.println("Strap Coonfigurator popin is not available on PDP page.");
			}
			
		
		
	}
	
	public void PopInClose()
	{
		boolean closebtn;
		closebtn = this.driver.findElement(popinclose).isDisplayed();
		if (closebtn == true)	
		{	
			this.driver.findElement(popinclose).click();			
			
		}
		
		else 
		{
			System.out.println("Country Popin is not there to close");
		}
		}
	
	public void VerifyText()
	{
		
		
			driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);	
			ModelNamePDP = this.driver.findElement(ReferenceName).getText();
			System.out.println(ModelNamePDP);
			RefSizePDP = this.driver.findElement(ReferenceSize).getText();
			System.out.println(RefSizePDP);
			RefNumPDP = this.driver.findElement(ReferenceNum).getText();
			System.out.println(RefNumPDP);
			RefPricePDP = this.driver.findElement(ReferencePrice).getText();
			System.out.println(RefPricePDP);
			ReftaxstatePDP = this.driver.findElement(ReferencePricetaxstate).getText();
			System.out.println(ReftaxstatePDP);		
		
	}
	
	public void minicartFunction() throws InterruptedException
	{
		
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		
		String cartnum1 = this.driver.findElement(EmptyMinicartIcon).getAttribute("innerHTML");
		if(cartnum1 == null) 
		{
			System.out.println("No items are there in the cart");
		}
		else 
		{
			driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
			System.out.println("Verifying Minicart Values");
			WebElement element = this.driver.findElement(MinicartIcon);
			Actions action = new Actions(driver);
			action.moveToElement(element).moveToElement(driver.findElement(MinicartIcon)).click().build().perform();
			mcheadername = this.driver.findElement(MinicartHeaderText).getText();
			System.out.println(mcheadername);
			mcReferenceDetails = this.driver.findElement(MinicartReferenceName).getText();
			System.out.println(mcReferenceDetails);
			mcReferenceSize = this.driver.findElement(MinicartReferenceSize).getText();
			System.out.println(mcReferenceSize);
			mcReferencePrice = this.driver.findElement(MinicartReferencePrice).getText();
			System.out.println(mcReferencePrice);
			mcReferenceItemsAddedNumber = this.driver.findElement(MinicartReferenceItemsAddedNumber).getText();
			System.out.println(mcReferenceItemsAddedNumber);
			mcTotalAmount = this.driver.findElement(MinicartTotalAmount).getText();
			System.out.println(mcTotalAmount);
			mcReferencePricetaxstate = this.driver.findElement(MinicartReferencePricetaxstate).getText();
			System.out.println(mcReferencePricetaxstate);
			mcCheckoutButton = this.driver.findElement(MinicartCheckoutButton).getText();
			System.out.println(mcCheckoutButton);
			ModelNameMC = this.driver.findElement(ReferenceName).getText();
			RefSizeMC = this.driver.findElement(ReferenceSize).getText();
			RefPriceMC = this.driver.findElement(ReferencePrice).getText();
			ReftaxstateMC = this.driver.findElement(ReferencePricetaxstate).getText();
			System.out.println("Minicart Reference Model Name is same as of PDP page and is " + ModelNameMC + " " + mcReferenceDetails.equals(ModelNameMC));
			System.out.println("Minicart Reference Size is same as of PDP page and is " + RefSizeMC + " " + mcReferenceSize.equals(RefSizeMC));
			System.out.println("Minicart Reference Price is same as of PDP page and is " + RefPriceMC + " " + mcReferencePrice.equals(RefPriceMC));
			//System.out.println("Minicart Tax state is same as of PDP page and is " + ReftaxstateMC + " " + ReftaxstatePDP.equals(ReftaxstateMC));
			String cartnum = this.driver.findElement(EmptyMinicartIcon).getAttribute("innerHTML");
			String cartnumval = this.driver.findElement(EmptyMinicartIcon).getText();
			System.out.println("Number of items in cart is " + cartnum);
			System.out.println("Number of items in cart is " + cartnumval);
		}
			
	}  	
	
	public void ClickSBIconPDPPage()
	{
		
			this.driver.findElement(MiniCartIconView).click();
		
	}
	
}
