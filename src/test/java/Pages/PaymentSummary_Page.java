package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.framework.PropertyReader;
public class PaymentSummary_Page {
	
WebDriver driver;
	
	
	//xpaths of the elements on PaymentSummary Page
		private By PaySumPageCardNumber = By.xpath("//*[@id='payment_card_number']");
		private By PaySumPageCardMonth = By.xpath("//*[@name ='payment_card_month']");
		private By PaySumPageCardMonthValue = By.xpath("//*[@value='08']");
		private By PaySumPageCardYear = By.xpath("//*[@name ='payment_card_year']");
		private By PaySumPageCardYearValue = By.xpath("//*[@value='2018']");
		private By PaySumPageCVV = By.xpath("//*[@id='payment_card_cvs']");
		private By PaySumPageCardHoldersName = By.xpath("//*[@id='payment_card_name']");
		private By PaySumPageconditionsLink = By.xpath("//*[@class='tab-card-payment']//*[@class='c-terms-condition']");
		private By PaySumPageSubmitButton = By.xpath("//*[@class='next-step-cta clearfix']//*[@type='submit']");
	
	
	public PaymentSummary_Page(WebDriver driverpassed)
	{
		this.driver= driverpassed;
		
	}
	
	
	public void PaymentSummaryPageValidation() 
	{
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		PropertyReader pr = new PropertyReader("src/test/resources/resources/config.properties");
		String cardnumber =   pr.getValue("cardnumber");
		String cardcvv= pr.getValue("cardcvv");
		String CardHolderName= pr.getValue("cardholdername");
		this.driver.findElement(PaySumPageCardNumber).sendKeys(cardnumber);
		System.out.println("Card number entered is: " + cardnumber);
		this.driver.findElement(PaySumPageCardMonth).click();
		this.driver.findElement(PaySumPageCardMonthValue).click();
		this.driver.findElement(PaySumPageCardYear).click();
		this.driver.findElement(PaySumPageCardYearValue).click();
		this.driver.findElement(PaySumPageCVV).sendKeys(cardcvv);
		System.out.println("CVV number entered is: " + cardcvv);
		this.driver.findElement(PaySumPageCardHoldersName).sendKeys(CardHolderName);
		System.out.println("Card Name entered is: " + CardHolderName);
		this.driver.findElement(PaySumPageconditionsLink).isDisplayed();
		this.driver.findElement(PaySumPageSubmitButton).click();
		
	}

}
