package Pages;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.fasterxml.jackson.annotation.JsonInclude.Value;
import com.framework.BaseDriver;

public class Delivery_Page extends BaseDriver {
	//private By PFPage1Heading = By.xpath("//*[@class='heading1 pf-boutique-heading']");
	WebDriver driver = null;
	
	//************************   X Path library starts   **********************//
	
	 		private By company_radio = By.xpath("(//*[@name='eFapiao_title_type'])[1]");
	 		private By individual_radio = By.xpath("//*[@class='icon icon-pf-radio-checked']");
			//*************************  eFapiao form ****************************//
	
			private By cdelivery_name = By.xpath("(//*[@class='c-delivery__name'])[1]"); 

			private By cdelivery_name2 = By.xpath("(//*[@class='c-delivery__name'])[2]"); 

			private By eFapiao_company = By.xpath("//*[@id='eFapiao_company']"); 

			private By eFapiao_individual = By.xpath("//*[@id='eFapiao_individual']"); 

			private By eFapiao_title = By.name("eFapiao_title"); 

			private By eFapiao_taxCode = By.xpath("//*[@id='eFapiao_taxCode']"); 

			private By eFapiao_email = By.xpath("//*[@id='eFapiao_email']"); 

			private By eFapiao_bankName = By.xpath("//*[@id='eFapiao_bankName']"); 

			private By eFapiao_bankAddress = By.xpath("//*[@id='eFapiao_bankAddress']"); 

			private By eFapiao_bankAccount = By.xpath("//*[@id='eFapiao_bankAccount']"); 

			private By eFapiao_bankPhone = By.xpath("//*[@id='eFapiao_bankPhone']"); 

			private By radio_checked5 = By.xpath("(//i[@class='icon icon-pf-radio-checked'])[5]");

			private By radio_checked6 = By.xpath("(//i[@class='icon icon-pf-radio-checked'])[6]"); 
			
			// ***************** shipping address **********************//

			private By shippingForm_title_address20 = By.xpath("//select[@id='shippingForm_address20']"); 

			private By shippingForm__chinese_lastName2 = By.xpath("//*[@id='shippingForm_lastName2']"); 

			private By shippingForm_chinese_firstName2 = By.xpath("//*[@id='shippingForm_firstName2']"); 

			private By shippingForm_lastName1 = By.xpath("//*[@id='shippingForm_lastName1']");
			
			private By shippingForm_firstName1 = By.xpath("//*[@id='shippingForm_firstName1']");
			
			private By shippingForm_address9 = By.xpath("//*[@id='shippingForm_address9']");
			//@FindBy(css = "#shippingForm_address9")
			//private WebElement shippingForm_address9;
			
			private By shippingForm_address7 = By.xpath("//*[@id='shippingForm_address7']");
			
			private By shippingForm_address8 = By.xpath("//*[@id='shippingForm_address8']");
			
			private By shippingForm_address5 = By.xpath("//*[@id='shippingForm_address5']");
			
			
			private By shippingForm_zip = By.xpath("//*[@id='shippingForm_zip']");
			
			private By shippingForm_phone = By.xpath("//*[@id='shippingForm_phone']");
			
			private By j_username = By.xpath("//*[@id='j_username']");
			
			private By shipping_form_submit_button = By.xpath("//input[@class='form-button valid']");
			
			private By ContinueNextStep = By.name("Submit");
			
			
			// ***************** Billing  address **********************//

			
			private By billingForm_firstName1 = By.xpath("//*[@id='billingForm_firstName1']"); 

			private By billingForm_address9 = By.xpath("//*[@id='billingForm_address9']"); 

				
			private By billingForm_address7 = By.xpath("//*[@id='billingForm_address7']"); 

			private By billingForm_address8 = By.xpath("//*[@id='billingForm_address8']"); 

			private By billingForm_address5 = By.xpath("//*[@id='billingForm_address5']"); 

			private By billingForm_zip = By.xpath("//*[@id='billingForm_zip']"); 

			private By billingForm_phone = By.xpath("//*[@id='billingForm_phone']"); 

			private By sendbutton = By.xpath("//*[@class='form-button valid']"); 

			private By fn_boutique_province = By.xpath("//*[@id='fn_boutique_province']"); 

			private By fn_boutique_city = By.xpath("//*[@id='fn_boutique_city']"); 

			private By billingForm_address20 = By.xpath("//*[@id='billingForm_address20']"); 

			private By billingForm_lastName2 = By.xpath("//*[@id='billingForm_lastName2']"); 

			private By billingForm_firstName2 = By.xpath("//*[@id='billingForm_firstName2']"); 

			private By billingForm_phone1 = By.xpath("//*[@id='billingForm_phone']");
			
			private By paymentpage_address_edit = By.xpath("(//*[@class='icon icon-pencil'])[2]");


	
	//************************   X Path library ends   **********************//
	
	
	public Delivery_Page(WebDriver driver){
		
		this.driver= driver;
		
	} 
	
	public void company_selection() throws InterruptedException
	{
		/*String str = driver.findElement(individual_radio).getAttribute("checked");
		if (str.equalsIgnoreCase("true"))
		{
		    System.out.println("Checkbox selected");
		}*/
		//Assert.assertTrue(driver.findElement(By.xpath("//*[@class='icon icon-pf-radio-checked']")).isSelected());
	//Assert.assertTrue(driver.findElement(individual_radio).isSelected());
	
	
	WebElement radio = driver.findElement(company_radio);
	radio.click();

	}
	
	
	
	public void eFapiao_form_without_tax() throws InterruptedException
	{
			
	this.driver.findElement(eFapiao_title).sendKeys("eFapiao form");
	//this.driver.findElement(eFapiao_taxCode).sendKeys("433636535");
	this.driver.findElement(eFapiao_email).sendKeys("abhatia52@sapient.com");
		
	}
	
	
	public void eFapiao_form() throws InterruptedException
		{
		this.driver.findElement(eFapiao_title).clear();		
		this.driver.findElement(eFapiao_title).sendKeys("eFapiao form");
		this.driver.findElement(eFapiao_taxCode).clear();
		this.driver.findElement(eFapiao_taxCode).sendKeys("433636535");
		this.driver.findElement(eFapiao_email).clear();
		this.driver.findElement(eFapiao_email).sendKeys("abhatia52@sapient.com");
			
		}
		
	public void shipping_form() throws InterruptedException{
		
		if (this.driver.findElement(shippingForm_title_address20).isDisplayed())
		{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		WebElement ele1 = driver.findElement(shippingForm_title_address20);
		js.executeScript("arguments[0].scrollIntoView();", ele1);
	
		WebElement dropdown = this.driver.findElement(shippingForm_title_address20);
		Select selectobj = new Select(dropdown);
		selectobj.selectByIndex(2); 
		}
		JavascriptExecutor js = (JavascriptExecutor)driver;
		this.driver.findElement(shippingForm_title_address20).sendKeys("此项");
		/*WebElement chln2 = driver.findElement(shippingForm__chinese_lastName2);
		js.executeScript("arguments[0].sendKeys('此项');", chln2);*/
		
		//js.executeScript(document.getElementById("shippingForm_lastName2").value = '请');
		//driver.findElement(shippingForm__chinese_lastName2).clear();
		driver.findElement(shippingForm__chinese_lastName2).sendKeys("请只输入");
		/*WebElement chfn2 = driver.findElement(shippingForm_chinese_firstName2);
		js.executeScript("arguments[0].sendKeys('此项');", chfn2);*/
		
		//driver.findElement(shippingForm_chinese_firstName2).clear();
		//js.executeScript("document.getElementById('shippingForm_firstName2').value = '请';");
		//driver.findElement(shippingForm_chinese_firstName2).clear();
		driver.findElement(shippingForm_chinese_firstName2).sendKeys("请只输");
		driver.findElement(shippingForm_lastName1).sendKeys("ashish");
		driver.findElement(shippingForm_firstName1).sendKeys("bhatia");
		
		//Actions act =new Actions(driver);
//		act.moveToElement(shippingForm_address9).build().perform();
		//Keys.chord(Keys.ARROW_DOWN, Keys.ARROW_DOWN);
		WebElement addr9 = this.driver.findElement(shippingForm_address9);
		
		js.executeScript("arguments[0].scrollIntoView();", addr9);
		waitForElementVisibility(5,addr9 );
		//WebDriverWait wait = new WebDriverWait(driver, 5);
		 //WebElement dropdown1 = driver.findElement(shippingForm_address9);
		
		Thread.sleep(5000);
		Select selectobj1 = new Select(addr9);
		selectobj1.selectByIndex(3);;
		
		WebElement dropdown2 = this.driver.findElement(shippingForm_address7);
		Select selectobj2 = new Select(dropdown2);
		selectobj2.selectByIndex(1);
		
		WebElement dropdown3 = this.driver.findElement(shippingForm_address8);
		Select selectobj3 = new Select(dropdown3);
		selectobj3.selectByIndex(1);
		
		this.driver.findElement(shippingForm_address5).sendKeys("abcd");
		
		WebElement dropdown4 = this.driver.findElement(shippingForm_zip);
		Select selectobj4 = new Select(dropdown4);
		selectobj4.selectByIndex(1);
		
		driver.findElement(shippingForm_phone).sendKeys("1234567890");
		WebElement inputBox = driver.findElement(By.id("j_username"));
		String textInsideInputBox = inputBox.getAttribute("value");
		Thread.sleep(2000);
		if (textInsideInputBox.isEmpty())
				driver.findElement(j_username).sendKeys("abhatia52@sapient.com");
		}
		
		
		
		
		
	   
	public void submit_form() {
		
	
		JavascriptExecutor js = (JavascriptExecutor)driver;
	//Thread.sleep(5000);
			WebElement submit = driver.findElement(By.name("Submit"));
			js.executeScript("arguments[0].scrollIntoView();", submit);
			//waitForElementVisibility(5,submit );
			//WebElement closeicon=this.driver.findElement(By.cssSelector("a[id$='close']"));
			
			driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("Submit")));
		    //closeicon.click();
			
			System.out.println("Is submit button presnt******"+submit.isDisplayed());
			//this.driver.findElement(ContinueNextStep).click();
			
			//WebDriverWait wait=new WebDriverWait(driver,5);
			//wait.until(ExpectedConditions.visibilityOfElementLocated(shipping_form_submit_button));
			
			js.executeScript("arguments[0].click();", submit);
			//submit.click();
			//driver.findElement(shipping_form_submit_button).click();
	
	
	}
	
		
//		act.sendKeys(Keys.ARROW_DOWN).keyDown(Keys.ARROW_DOWN).build().perform();
//		
//		driver.findElement(shipping_form_submit_button).click();


		public void tax_code_check() throws InterruptedException{
			String actualerrormessage=driver.findElement(By.cssSelector("div[id$='taxCode'] span")).getText();
			System.out.println("actual error messag eis "+actualerrormessage);
	
		Assert.assertEquals("此项为必填项",actualerrormessage);
		}
	
		
			
	public void back_to_shipping_from_payment() throws InterruptedException{
		
		eFapiao_form();
		submit_form();
		Thread.sleep(10000);
		
		
		WebElement ele = driver.findElement(paymentpage_address_edit);
		waitForElementVisibility(10, ele);
		ele.click();
		//this.driver.findElement(paymentpage_address_edit).click();
		
	}
	
	public void waitForElementVisibility(int timeInSec, WebElement elem) {
		WebDriverWait wait = new WebDriverWait(driver, timeInSec);
		wait.until(ExpectedConditions.visibilityOf(elem));		
	}
	
	public void retention() throws InterruptedException{
		
		/*Actions act= new Actions(driver);
		act.sendKeys(Keys.PAGE_UP).sendKeys(Keys.PAGE_UP).sendKeys(Keys.PAGE_UP).sendKeys(Keys.PAGE_UP).sendKeys(Keys.PAGE_UP).sendKeys(Keys.PAGE_UP).build().perform();
		driver.findElement(eFapiao_title).isDisplayed();*/
		
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    System.out.println(tabs.size());
	    driver.switchTo().window(tabs.get(5));
	    
		JavascriptExecutor js = (JavascriptExecutor)driver;
	/*	WebElement addr8 = driver.findElement(shippingForm_address8); 
		waitForElementVisibility(10 , addr8);*/
	   Thread.sleep(3000);
	//			WebElement eFapiao_tax_ele = driver.findElement(By.xpath("//*[@id='eFapiao_taxCode']"));
	//			js.executeScript("arguments[0].scrollIntoView();", eFapiao_tax_ele);
			//	js.executeScript("window.scrollBy(20,20)");
				
				//WebElement sf_title = driver.findElement(eFapiao_title);
				//waitForElementVisibility(10 , sf_title);
				js.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
		        System.out.println("test*************");
				
		//String title = driver.findElement(eFapiao_title).getText();
		      //input[@id='eFapiao_title']
		WebElement eFapiao_title=driver.findElement(By.xpath("//input[@id='eFapiao_title']"));
		String a=eFapiao_title.getAttribute("value");
		System.out.println(a + " empty a");
		       
		Assert.assertEquals("eFapiao form", a);
		System.out.println("passed ..............");
		
		//************************************************************
		
		 WebElement eFapiao_taxCode=driver.findElement(By.xpath("//input[@id='eFapiao_taxCode']"));
		 String b=eFapiao_taxCode.getAttribute("value");
		 System.out.println(b + " empty b");
		
		 Assert.assertEquals("433636535", b);
		
		//************************************************************
		 
		 WebElement eFapiao_email=driver.findElement(By.xpath("//input[@id='eFapiao_email']"));
		 String c=eFapiao_email.getAttribute("value");
		 System.out.println(c + " empty c");
		
		 Assert.assertEquals("abhatia52@sapient.com", c); 
	
		
		
	}	
	}

