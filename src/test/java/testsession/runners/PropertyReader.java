package testsession.runners;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/*import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
*/
public class PropertyReader {

	Properties pr;
	
	public PropertyReader(String filepath) {
		
		pr = new Properties();
		try {
			pr.load(new FileInputStream(new File(filepath)));
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
	}
	
	public String getValue(String key) {
		return pr.getProperty(key);
		
		
		
	}
	
}
