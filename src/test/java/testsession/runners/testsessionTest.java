package testsession.runners;

import java.io.File;

import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Parameters;

import com.cucumber.listener.Reporter;

//import com.cartier.BaseDriver;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
//import cucumber.api.testng.AbstractTestNGCucumberTests;


@RunWith(Cucumber.class)
/*@ExtendedCucumberOptions(
		jsonReport = {"target/CartierAutomationTestReport.json"},
		overviewReport = true,
customTemplatesPath = {"src/test/resources/templates/overview.ftlh"}, // Point to local folder with custom templates
outputFolder = "target")*/

@CucumberOptions
(
		
	features = {"src\\test\\resources\\featurefiles"},
	glue = {"com.sapient.cartier.stepdefination"},
	plugin= {"html:target/cucumber-html-report",
		    "json:target/cucumber.json", "pretty:target/cucumber-pretty.txt",
		    "usage:target/cucumber-usage.json", "junit:target/cucumber-results.xml" },
	monochrome = true
	, tags= {"@AEMsearch"}
		
	
	
		 
	
	/*"pretty","html:target/CartierAutomationTestReport"
	,"json:target/CartierAutomationTestReport.json"
	,"junit:target/CartierAutomationTestReport.xml"
	, "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"
	}, */
	
	)

/*public class testsessionTest {
	@AfterClass
	public static void teardown() {

		Reporter.loadXMLConfig(new File("target/extent-config.xml"));
		Reporter.setSystemInfo("user", System.getProperty("ashish.bhatia"));
		Reporter.setSystemInfo("os", "Windows 10");
		Reporter.setTestRunnerOutput("Sample test runner output message");
	}


}

*/

/*@RunWith(Cucumber.class)
@CucumberOptions
(
	features = {"src\\test\\resources\\featurefiles"},
	glue = {"com.sapient.cartier.stepdefination"},
	plugin= {"pretty","html:target/CartierAutomationTestReport"
			,"json:target/CartierAutomationTestReport.json"
			,"junit:target/CartierAutomationTestReport.xml" , "html:target/cucumber-html-report", "json:target/cucumber.json"}, 
	monochrome = true
	, tags= {"@Regression"}
			
		  
) */

public class testsessionTest  {


}

