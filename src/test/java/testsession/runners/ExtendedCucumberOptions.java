package testsession.runners;

public @interface ExtendedCucumberOptions {

	String[] jsonReport();

	boolean overviewReport();

	String[] customTemplatesPath();

	String outputFolder();


}
