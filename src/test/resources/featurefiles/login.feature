Feature:  E commerce

@Regression 
Scenario: To test eFapiao form functionality        
			Given User is on home page
    	When User selects item and redirected to shopping bag 
	    Then After submitting User should be redirected to delivery page where Individual form should be selected by default
	    When User selects Company radio button and doesnt enter tax code while entering efapiao form 
     	Then tax code should show error message 
    	Given User is on delivery page, enters all shipping details
	    When User submits company form and comes back from payment page  
	    Then form details should retain   
	    
@Test 
Scenario: To test Sitemap links response        
		 	When User opens sitemap links and clicks on each link
	    Then User gets response  	    
	    

		
@TEST2		
Scenario: Verify validation when user enter wrong username or password 
	 Given user is on login page 
	 When enter incorrect username & password 
	 And click on Login 
	 Then User should display the validation message
	  When efapio form gets filled
	  
	  
@AEMsearch
Scenario:To verify AEM Search Functionality
	Given User is on home page
	When User enter any keyword on search field and press enter
	Then User should be redirected to search results Page 
	Given User is on AEM search results page
	When User selects Sold Online radio button
	Then All sellable items are showing
	When User checks vignettes availability
	And User check for more information text
	Then User clicks on Vignettes for available vignettes
	When User clicks on view more products on any bucket
	Then Few more products opened
	Then User verifies all product properties
	When User clicks on any of the product
	Then PDP page opens
	
	
	
	
	  
	  