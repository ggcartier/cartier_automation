$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "line": 1,
  "name": "E commerce",
  "description": "",
  "id": "e-commerce",
  "keyword": "Feature"
});
formatter.before({
  "duration": 43466464711,
  "status": "passed"
});
formatter.scenario({
  "line": 31,
  "name": "To verify AEM Search Functionality",
  "description": "",
  "id": "e-commerce;to-verify-aem-search-functionality",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 30,
      "name": "@AEMsearch"
    }
  ]
});
formatter.step({
  "line": 32,
  "name": "User is on home page",
  "keyword": "Given "
});
formatter.step({
  "line": 33,
  "name": "User enter any keyword on search field and press enter",
  "keyword": "When "
});
formatter.step({
  "line": 34,
  "name": "User should be redirected to search results Page",
  "keyword": "Then "
});
formatter.step({
  "line": 35,
  "name": "User is on AEM search results page",
  "keyword": "Given "
});
formatter.step({
  "line": 36,
  "name": "User selects Sold Online radio button",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "All sellable items are showing",
  "keyword": "Then "
});
formatter.step({
  "line": 38,
  "name": "User checks vignettes availability",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "User check for more information text",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "User clicks on Vignettes for available vignettes",
  "keyword": "Then "
});
formatter.step({
  "line": 41,
  "name": "User clicks on view more products on any bucket",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "Few more products opened",
  "keyword": "Then "
});
formatter.step({
  "line": 43,
  "name": "User verifies all product properties",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "User clicks on any of the product",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "PDP page opens",
  "keyword": "Then "
});
formatter.match({
  "location": "stepdefination.home_page()"
});
formatter.result({
  "duration": 122184636,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.user_enter_any_keyword()"
});
formatter.result({
  "duration": 56996832089,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.user_should_be_redirected_to_search_results_Page()"
});
formatter.result({
  "duration": 56889,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.user_is_on_AEM_search_results_page()"
});
formatter.result({
  "duration": 60901,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.User_selects_Sold_Online_radio_button()"
});
formatter.result({
  "duration": 7009706490,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.All_sellable_items_are_showing()"
});
formatter.result({
  "duration": 56525,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.User_check_vignettes()"
});
formatter.result({
  "duration": 2052662730,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.user_check_for_more_information_text()"
});
formatter.result({
  "duration": 32274981,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.User_click_vignette()"
});
formatter.result({
  "duration": 53607,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.user_clicks_on_view_more_products_on_any_bucket()"
});
formatter.result({
  "duration": 4029541395,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.some_more_products_opened()"
});
formatter.result({
  "duration": 119977,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.user_verifies_all_product_properties()"
});
formatter.result({
  "duration": 2057949752,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.user_clicks_on_any_of_the_product()"
});
formatter.result({
  "duration": 23483943832,
  "status": "passed"
});
formatter.match({
  "location": "stepdefination.pdp_page_opens()"
});
formatter.result({
  "duration": 75122,
  "status": "passed"
});
formatter.after({
  "duration": 3240602206,
  "status": "passed"
});
});